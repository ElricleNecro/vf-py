# Add this in your Xshrc
# 	source /path/to/vf.sh
#
# With ZSH, this script assume that compinit has been loaded!
#
# 	autoload -U compinit
# 	compinit

export VF_HOME=${VF_HOME:-$(dirname $0)}
VF_OPTIONS=($(python3 $VF_HOME/vf.py -C))

vf() {
	eval $(python3 $VF_HOME/vf.py $@)
}

# -- ZSH ----------------------------------------------------------------------

__vf_zsh_make_comp_list()
{
	compadd $VF_OPTIONS
	_files
}

__vf_zsh_complete()
{
	compdef __vf_zsh_make_comp_list vf
}

# -- BASH ---------------------------------------------------------------------

__bash_make_comp_list()
{	
	COMPREPLY=( $(compgen -W "${VF_OPTIONS[*]}") )
	return 0
}

__bash_complete()
{
	complete -df -F __bash_make_comp_list vf
}

# -- MAIN ---------------------------------------------------------------------

T_SHELL=$(ps -p $$ | tail -1 | awk '{print $NF}' | sed -e 's/-//g')

if [ "X$T_SHELL" = "Xzsh" ]; then
	__vf_zsh_complete
fi

if [ "X$T_SHELL" = "Xbash" ]; then
	__bash_complete
fi
