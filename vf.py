#!/usr/bin/env python
# encoding: utf-8

import os

import argparse as ag
import pathlib as pt
import yaml as y

ALIAS_FILE = pt.Path(os.getenv("HOME")) / ".vf-py" / "vf.yml"
CONF_FILE = pt.Path(os.getenv("HOME")) / ".vf-py" / "conf.yml"
HIST_FILE = pt.Path(os.getenv("HOME")) / ".vf-py" / "history.yml"


class Vf:
    def __init__(self, alias, conf, history):
        if not conf.exists():
            a = conf.open("w")
            a.close()
        self._conf = y.load(conf.open("r"), Loader=y.SafeLoader)

        if not alias.exists():
            a = alias.open("w")
            a.write("{}")
            a.close()
        self._alias = y.load(alias.open("r"), Loader=y.SafeLoader)

        if not history.exists():
            a = history.open("w")
            a.close()
        self._history = y.load(history.open("r"), Loader=y.SafeLoader)

    def __call__(self, name):
        return self.get(name)

    def get(self, name):
        try:
            return self.get_alias(name)
        except:
            return "cd " + name

    def get_alias(self, alias):
        if alias not in self._alias:
            raise ValueError(
                alias + " not in " + ", ".join([a for a in self._alias])
            )
        return "cd " + self._alias[alias]

    def get_all_alias(self):
        res = ""
        for a in self._alias:
            res += "echo %s : %s;\n" % (a, self._alias[a])
        return res

    def get_alias_list(self):
        return " ".join(self._alias)
        # return "\n".join([a for a in self._alias])

    def register(self, alias):
        self._alias[alias] = os.getcwd()
        y.dump(
            self._alias,
            ALIAS_FILE.open("w")
        )


if __name__ == '__main__':
    args = ag.ArgumentParser()
    args.add_argument(
        "dir",
        type=str,
        nargs='*',
        default=[os.getenv("HOME")],
        # dest="dir",
    )
    args.add_argument(
        "-c",
        "--config",
        default=CONF_FILE,
        type=str,
    )
    args.add_argument(
        "-a",
        "--alias",
        # type=str,
        action='store_true',
        help="Use the given alias."
    )
    args.add_argument(
        "-l",
        "--list",
        action='store_true',
        help="List all known aliases."
    )
    args.add_argument(
        "-s",
        "--save",
        type=str,
        help="Save current directory under the given alias."
    )
    args.add_argument(
        "-C",
        action='store_true',
    )
    args = args.parse_args()

    # If the home directory does not exists, create it:
    if not ALIAS_FILE.parent.exists():
        ALIAS_FILE.parent.mkdir()

    vf = Vf(ALIAS_FILE, CONF_FILE, HIST_FILE)

    if args.C:
        print(vf.get_alias_list())
    elif args.list:
        print(vf.get_all_alias())
    elif args.alias:
        print(vf.get_alias(args.dir[0]))
    elif args.save:
        vf.register(args.save)
    else:
        print(vf(args.dir[0]))
